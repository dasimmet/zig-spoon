const std = @import("std");
const Builder = std.build.Builder;

const Exe = struct {
    name: []const u8,
    src: []const u8,
    libC: bool = false,
};

pub fn build(b: *Builder) void {
    const target = b.standardTargetOptions(.{});
    const mode = b.standardOptimizeOption(.{});

    const tests = b.addTest(.{
        .root_source_file = .{ .path = "test_main.zig" },
        .target = target,
        .optimize = mode,
    });
    const test_step = b.step("test", "Run all tests");
    test_step.dependOn(&tests.step);

    const spoon = b.addModule("spoon", .{
        .source_file = .{ .path = "import.zig" },
    });

    const install = b.getInstallStep();
    const fmt = b.step("fmt", "format sources");
    fmt.dependOn(&b.addFmt(.{
        .paths = &.{ "example", "lib", "build.zig" },
    }).step);

    for ([_]Exe{
        .{
            .name = "menu",
            .src = "example/menu.zig",
        },
        .{
            .name = "menu-libc",
            .src = "example/menu.zig",
            .libC = true,
        },
        .{
            .name = "input-demo",
            .src = "example/input-demo.zig",
        },
        .{
            .name = "colours",
            .src = "example/colours.zig",
        },

        .{ .name = "table-256-colours", .src = "example/table-256-colours.zig" },
    }) |it| {
        const exe = b.addExecutable(.{
            .name = it.name,
            .root_source_file = .{ .path = it.src },
            .target = target,
            .optimize = mode,
        });
        exe.addModule("spoon", spoon);
        if (it.libC) {
            exe.linkLibC();
        }
        install.dependOn(&b.addInstallArtifact(exe, .{}).step);
    }
}
